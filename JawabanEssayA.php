<?php
// $string = "7+4";


function hitung($string) {    
    $operator = "";
    $num1 = "";
    $num2 = "";
    $hasil = 0;

    // cek operator
    for($i=0; $i<strlen($string); $i++) {
        if($string[$i]=="*") {
            $operator .= "*";
        } else if($string[$i]=="+") {
            $operator .= "+";
        } else if($string[$i]==":") {
            $operator .= ":";
        } else if($string[$i]=="%") {
            $operator .= "%";
        } else if($string[$i]=="-") {
            $operator .= "-";
        }
    }
    
    for($i=0;$i<strpos($string,$operator);$i++) {
        $num1 .= $string[$i];
    }
    for($i=strpos($string,$operator)+1;$i<strlen($string);$i++) {
        $num2 .= $string[$i];
    }

    if($operator=="*") {
        $hasil = intval($num1)*intval($num2);
    } else if($operator=="+") {
        $hasil = intval($num1)+intval($num2);
    } else if($operator==":") {
        $hasil = intval($num1)/intval($num2);
    } else if($operator=="%") {
        $hasil = intval($num1)%intval($num2);
    } else if($operator=="-") {
        $hasil = intval($num1)-intval($num2);
    }
    echo $hasil . "<br>";
}


// TEST CASES
echo hitung("102*2"); //204
echo hitung("2+3"); //5
echo hitung("100:25"); //4
echo hitung("10%2"); //0
echo hitung("99-2"); //97



?>